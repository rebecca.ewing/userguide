Mobile Apps
===========

There are two unofficial apps for mobile devices. Install one of these apps to
view GW alerts on your smart phone or tablet.

* `Gravitational Wave Events
  <https://apps.apple.com/us/app/gravitational-wave-events/id1441897107>`_ for
  `iOS <https://apps.apple.com/us/app/gravitational-wave-events/id1441897107>`_
  by Peter Kramer

* `Chirp <https://www.laserlabs.org/chirp.php>`_ for
  `iOS <https://apps.apple.com/app/chirp-gravitational-wave-app/id1484328193>`_
  or `Android <https://play.google.com/store/apps/details?id=org.laserlabs.chirp>`_
  by Laser Labs
